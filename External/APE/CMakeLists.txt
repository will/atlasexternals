# Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
#
# CMake configuration building APE as part of the release build.
# To be kept in sync with the requirements file of the package.
#

# Set the name of the package:
atlas_subdir( APE )

# Silence ExternalProject warnings with CMake >=3.24.
if( POLICY CMP0135 )
   cmake_policy( SET CMP0135 NEW )
endif()

# External dependencies:
find_package( TBB )

# Declare where to get Acts from.
set( ATLAS_APE_SOURCE
   "URL;https://github.com/samikama/APE/tarball/b47d155;URL_MD5;73d4d2d61d10f55454738d9ec50ed9d3"
   CACHE STRING "The source for APE" )
mark_as_advanced( ATLAS_APE_SOURCE )

# Decide whether / how to patch the APE sources.
set( ATLAS_APE_PATCH "" CACHE STRING "Patch command for APE" )
set( ATLAS_APE_FORCEDOWNLOAD_MESSAGE
   "Forcing the re-download of APE (2022.10.11.)"
   CACHE STRING "Download message to update whenever patching changes" )
mark_as_advanced( ATLAS_APE_PATCH ATLAS_APE_FORCEDOWNLOAD_MESSAGE )

# Directory to put temporary build results into:
set( _buildDir "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/APEBuild" )

# Build the package for the build area:
ExternalProject_Add( APE
   PREFIX "${CMAKE_BINARY_DIR}"
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   ${ATLAS_APE_SOURCE}
   ${ATLAS_APE_PATCH}
   CMAKE_CACHE_ARGS
   -DTBB_INSTALL_DIR:PATH=${TBB_INSTALL_DIR}
   -DYAMPL_DIR:PATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   -DBOOST_ROOT:PATH=${BOOST_LCGROOT}
   -DBOOST_INCLUDEDIR:PATH=${BOOST_INCLUDEDIR}
   -DCMAKE_PREFIX_PATH:PATH=${YAMLCPP_LCGROOT}
   -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   -DCMAKE_SYSTEM_IGNORE_PATH:STRING=/usr/include;/usr/lib;/usr/lib32;/usr/lib64
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( APE forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "${ATLAS_APE_FORCEDOWNLOAD_MESSAGE}"
   DEPENDERS download )
ExternalProject_Add_Step( APE purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results of APE"
   DEPENDEES download
   DEPENDERS patch )
ExternalProject_Add_Step( APE forceconfigure
   COMMAND ${CMAKE_COMMAND} -E remove -f "<BINARY_DIR>/CMakeCache.txt"
   COMMENT "Forcing the (re-)configuration of APE"
   DEPENDEES update
   DEPENDERS configure
   ALWAYS 1 )
ExternalProject_Add_Step( APE buildinstall
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}/lib/cmake"
   COMMAND ${CMAKE_COMMAND} -E copy_directory "${_buildDir}/" "<INSTALL_DIR>"
   COMMENT "Installing APE into the build area"
   DEPENDEES install )
add_dependencies( APE yampl )
add_dependencies( Package_APE APE )

# Install APE:
install( DIRECTORY "${_buildDir}/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

# Install its find-module:
install( FILES "cmake/FindAPE.cmake"
   DESTINATION "${CMAKE_INSTALL_CMAKEDIR}/modules" )
