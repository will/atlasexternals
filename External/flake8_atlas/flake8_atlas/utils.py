# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
"""Utilities for flake8 plugins"""

from flake8_atlas import __version__

def flake8_atlas(f):
   """Default decorator for flake8 plugins"""
   f.name = 'flake8_atlas'
   f.version = __version__
   if not hasattr(f, 'off_by_default'):
      f.off_by_default = False
   return f

def off_by_default(f):
   """Decorator to disable plugin by default (use --enable-extensions)"""
   f.off_by_default = True
   return f
