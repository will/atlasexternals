# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
from flake8_atlas.test.testutils import Flake8Test

class Test(Flake8Test):
   """
   Test OutputLevel checker
   """
   def test_assign(self):
      """Setting of OutputLevel is not allowed"""
      self.assertFail('c.OutputLevel = 0', 'ATL900')

   def test_setattr(self):
      """Use of setattr is not allowed"""
      self.assertFail('setattr(c, "OutputLevel", 0)', 'ATL900')

   def test_read(self):
      """Reading of OutputLevel is OK"""
      self.assertPass('if c.OutputLevel > 0: pass', 'ATL900')

   def test_kwargs(self):
      """Setting of OutputLevel via kwargs is not allowed"""
      self.assertFail('MyAlg(OutputLevel = 0)', 'ATL900')
      self.assertFail('MyAlg(prop1 = 1, OutputLevel = 0, prop2 = 2)', 'ATL900')
      self.assertPass('MyAlg(prop1 = 1)', 'ATL900')
