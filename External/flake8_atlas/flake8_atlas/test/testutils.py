# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration

import unittest
import ast
import itertools
import flake8.options.parse_args

class Flake8Test(unittest.TestCase):
   """Unit test case for flake8 checker"""

   plugins, _ = flake8.options.parse_args.parse_args([])

   def _run(self, source, checker):
      """Run AST or line-based checker and return list of errors"""

      # Find the relevant plugin:
      p = next(filter(lambda p : p.entry_name == checker,
                      itertools.chain(self.plugins.all_plugins(), self.plugins.disabled)))
      kwargs = {}
      if 'tree' in p.parameters:
         kwargs['tree'] = ast.parse(source)
         if 'lines' in p.parameters:
            kwargs['lines'] = source.splitlines()
         chk = p.obj(**kwargs).run()
      else:
         chk = p.obj(source)
      return list(chk)

   def assertFail(self, source, checker):
      """Test that checker returns an error on source"""
      codes = self._run(source, checker)
      self.assertEqual(len(codes), 1)

   def assertPass(self, source, checker):
      """Test that checker passes on source"""
      codes = self._run(source, checker)
      self.assertListEqual(codes, [])
