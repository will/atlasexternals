
# Set up the project.
cmake_minimum_required( VERSION 3.6 )
project( AtlasLCGTest VERSION 1.0.0 )

# Set the version of LCG to test the code with.
set( LCG_VERSION_NUMBER 100 CACHE STRING
   "Version number for the LCG release to test" )
set( LCG_VERSION_POSTFIX "" CACHE STRING
   "Post-fix to the LCG version number" )

# Set up the LCG release.
set( LCG_DIR "${CMAKE_SOURCE_DIR}/.." CACHE PATH
   "Directory holding the AtlasLCG code" )
find_package( LCG ${LCG_VERSION_NUMBER} REQUIRED EXACT )

# Find all of the find-modules in the AtlasLCG code.
file( GLOB _findModules
   RELATIVE "${LCG_DIR}/modules"
   "${LCG_DIR}/modules/Find*.cmake" )

# Obsolete modules that throw an error.
set ( _obsolete "PythonLibs;PythonInterp")

# Use each and every one of them on the selected LCG release.
foreach( _moduleFile ${_findModules} )
   string( REGEX REPLACE "^Find(.*)\.cmake$" "\\1"
      _module ${_moduleFile} )

   if ( NOT ${_module} IN_LIST _obsolete )
      find_package( ${_module} )
   endif()
endforeach()

# Generate an environment setup script.
lcg_generate_env( SH_FILE ${CMAKE_BINARY_DIR}/env_setup.sh )

# Print the configured RPM dependencies.
get_property( _rpms GLOBAL PROPERTY ATLAS_EXTERNAL_RPMS )
if( _rpms )
   message( STATUS "RPM dependencies: ${_rpms}" )
else()
   message( STATUS "No RPM dependencies were set up" )
endif()
unset( _rpms )
